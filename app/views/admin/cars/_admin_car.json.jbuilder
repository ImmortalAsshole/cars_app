json.extract! admin_car, :id, :manufactuture, :model, :year, :created_at, :updated_at
json.url admin_car_url(admin_car, format: :json)
